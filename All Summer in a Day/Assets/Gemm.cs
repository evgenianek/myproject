﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gemm : MonoBehaviour
{
    public GameObject Text12;
    private float timer;
    public GameObject gemm;
    // Start is called before the first frame update
    void Start()
    {
        Text12.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text12.SetActive(true);
            if (timer > 40)
            {
                Text12.SetActive(false);
                gemm.transform.position = new Vector3(-1.5049f, -4.04f, 4.8201f);
            }
        }
    }
}
