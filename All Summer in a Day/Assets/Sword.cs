﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public GameObject Text8;
    private float timer;
    public GameObject swordd;
    // Start is called before the first frame update
    void Start()
    {
        Text8.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text8.SetActive(true);
            if (timer > 40)
            {
                Text8.SetActive(false);
                swordd.transform.position = new Vector3(-6.599f, -6.631526f, -1.29f);
            }
        }
    }
}
