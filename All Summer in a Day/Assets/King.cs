﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : MonoBehaviour
{
    public GameObject Text16;
    public GameObject Text18;
    private float timer;
    public GameObject king;
    // Start is called before the first frame update
    void Start()
    {
        Text16.SetActive(false);
        Text18.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text16.SetActive(true);
            if (timer > 40)
            {
                Text16.SetActive(false);
                Text18.SetActive(true);
                king.transform.position = new Vector3(1.991f, -5.67f, -15.493f);
            }
            if (timer > 60)
            {
                Text18.SetActive(false);
            }

        }
    }
}
