﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkieTalkei : MonoBehaviour
{

    public GameObject Text11;
    private float timer;
    public GameObject walkie;
    // Start is called before the first frame update
    void Start()
    {
        Text11.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text11.SetActive(true);
            
            if (timer > 40)
            {
                Text11.SetActive(false);
                walkie.transform.position = new Vector3(-1.503867f, -4.164326f, 3.245701f);
            }
        }
    }
}
