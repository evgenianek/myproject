﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldRaDio : MonoBehaviour
{
    public GameObject Text6;
    private float timer;
    public GameObject radio;
    // Start is called before the first frame update
    void Start()
    {
        Text6.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text6.SetActive(true);
            if (timer > 40)
            {
                Text6.SetActive(false);
                radio.transform.position = new Vector3(-5.793359f, -6.65736f, -1.080871f);
            }
        }
    }
}
