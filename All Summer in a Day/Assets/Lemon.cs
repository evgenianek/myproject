﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;
using System.Windows;
using UnityEngine.UI;
     
public class Lemon : MonoBehaviour
{
    public Button Button2;
    public GameObject Text2;
    private float timer;
    public GameObject lemon;

    // Start is called before the first frame update
    void Start()
    {
        Text2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button2.transform.position += new Vector3(58.8f, -0.9278f, -535.9694f);
            //Button2.GetComponent<RectTransform>().localPosition = new Vector3(-4.393f, -0.44f, 82.26f);
            Text2.SetActive(true);
            if (timer > 40)
            {
                Text2.SetActive(false);
                lemon.transform.position = new Vector3(0.0664f, 6.96f, 0.3442f);
            }
        }
    }
}
