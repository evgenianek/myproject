﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulk : MonoBehaviour
{
    public GameObject Text15;
    private float timer;
    public GameObject blk;
    // Start is called before the first frame update
    void Start()
    {
        Text15.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text15.SetActive(true);
            if (timer > 40)
            {
                Text15.SetActive(false);
                blk.transform.position = new Vector3(-1.588267f, -4.150102f, -1.127818f);
            }
        }
    }
}
