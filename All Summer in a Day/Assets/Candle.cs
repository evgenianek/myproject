﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candle : MonoBehaviour
{
    public GameObject Text13;
    private float timer;
    public GameObject cand;
    // Start is called before the first frame update
    void Start()
    {
        Text13.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text13.SetActive(true);
            if (timer > 40)
            {
                Text13.SetActive(false);
                cand.transform.position = new Vector3(4.829657f, -6.46494f, 8.800793f);
            }
        }
    }
}
