﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLPlate : MonoBehaviour
{
    public GameObject Text5;
    private float timer;
    public GameObject GLplate;
    // Start is called before the first frame update
    void Start()
    {
        Text5.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text5.SetActive(true);
            if (timer > 40)
            {
                Text5.SetActive(false);
                GLplate.transform.position = new Vector3(4.406f, 5.141235f, 5.58f);
            }
        }
    }
}
