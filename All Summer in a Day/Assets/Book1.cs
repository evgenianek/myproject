﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Book1 : MonoBehaviour
{
    public Button Button3;
    public GameObject Text3;
    private float timer;
    public GameObject book1;
    // Start is called before the first frame update
    void Start()
    {
        Text3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text3.SetActive(true);
            if (timer > 40)
            {
                Text3.SetActive(false);
                book1.transform.position = new Vector3(-2.892405f, -7.026258f, 5.619503f);
            }
        }
    }
}
