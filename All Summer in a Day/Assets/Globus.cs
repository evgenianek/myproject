﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globus : MonoBehaviour
{
    public GameObject Text9;
    private float timer;
    public GameObject globe;
    // Start is called before the first frame update
    void Start()
    {
        Text9.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text9.SetActive(true);
            if (timer > 40)
            {
                Text9.SetActive(false);
                globe.transform.position = new Vector3(-5.564955f, 5.159612f, 9.119659f);
            }
        }
    }
}
