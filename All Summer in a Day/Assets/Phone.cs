﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phone : MonoBehaviour
{
    public GameObject Text10;
    private float timer;
    public GameObject phone;
    // Start is called before the first frame update
    void Start()
    {
        Text10.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text10.SetActive(true);
            if (timer > 40)
            {
                Text10.SetActive(false);
                phone.transform.position = new Vector3(-1.906685f, -6.992458f, -5.169531f);
            }
        }
    }
}
