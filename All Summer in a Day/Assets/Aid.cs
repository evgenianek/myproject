﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aid : MonoBehaviour
{
    public GameObject Text14;
    private float timer;
    public GameObject aid;
    // Start is called before the first frame update
    void Start()
    {
        Text14.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text14.SetActive(true);
            if (timer > 40)
            {
                Text14.SetActive(false);
                aid.transform.position = new Vector3(-5.714818f, 0.7512152f, 5.477735f);
            }
        }
    }
}
