﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForText4 : MonoBehaviour
{
    public GameObject Text4;
    private float timer;
    public GameObject cameraobj;
    // Start is called before the first frame update
    void Start()
    {
        Text4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<OVRGrabbable>().isGrabbed)
        {
            timer += Time.deltaTime;
            //Button3.transform.position += new Vector3(46.87f, 0.09f, -539.46f);
            //Button3.GetComponent<RectTransform>().localPosition = new Vector3(46.87f, 0.09f, -539.46f);
            Text4.SetActive(true);
            if (timer > 40)
            {
                Text4.SetActive(false);
                cameraobj.transform.position = new Vector3(-1.993f, -3.808f, 4.991f);
            }
        }
    }
}
